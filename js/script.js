(function ($, Drupal) {
    $(document).ready(function () {
        $(document).on('click', '.tabledrag-toggle-weight', function() {
            checkWeightToggle();
        });
    });

    $(window).on('load',function(){
        checkWeightToggle();
    });

    function checkWeightToggle(){
        console.log('check weight toggle');
        $('table').removeClass('weight-toggled');
        $('.action-link--icon-hide').each(function () {
            console.log('each');
            var $parent = $(this).closest('.layer-wrapper');
            $('>table',$parent).addClass('weight-toggled');
        });
    }
})(jQuery, Drupal);