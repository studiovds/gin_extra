# Gin Extra

## Why

I created Gin Extra to improve the already wonderfull Gin admin theme. I noticed that there where some inconsistencies on the node edit view. I improved:

* Paragraph Experimental Field
* Fieldset Field
* Media Field
* And some minor changes to improve use of the node edit view

## Beta

This is a beta version. Please let me know if something is not working as expected. Or if you find something in that I should change too. Send an e-mail to gin_extra@studiovds.nl

## Credits

Created by Joost van der Steen of Studio VDS https://studiovds.nl